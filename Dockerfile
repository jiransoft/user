# 编译
FROM golang:1.17 as builder

ENV GOPROXY=https://goproxy.cn,https://goproxy.io,direct \
    GO111MODULE=on \
    CGO_ENABLED=0 \
    GOOS=linux \
    GOARCH=amd64 \
    GONOPROXY=gitee.com/jiransoft

WORKDIR /go/src/user
RUN go env -w GOPROXY=https://goproxy.cn,https://goproxy.io,direct
COPY / ./

RUN go mod tidy && go build -o bin/main cmd/user/main.go

# 打包
FROM registry.cn-qingdao.aliyuncs.com/sxs/jh-alpine:1.0

COPY --from=builder /go/src/user/bin/main /

#入口
ENTRYPOINT ["./main"]


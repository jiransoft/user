# Go parameters
GOCMD=go
GOBUILD=$(GOCMD) build
VERSION=v0.1.0
MODULENAME=user
DOCKER_IMAGE_URL=registry.cn-hangzhou.aliyuncs.com/sxs/$(MODULENAME)

all:build
b:
	rm -rf bin && $(GOBUILD) -o bin/$(MODULENAME) cmd/$(MODULENAME)/main.go
	sudo docker build -t $(MODULENAME):$(VERSION) .
	sudo docker tag $(MODULENAME):$(VERSION) $(DOCKER_IMAGE_URL):$(VERSION)
	sudo docker tag $(MODULENAME):$(VERSION) $(DOCKER_IMAGE_URL):latest
	sudo docker push $(DOCKER_IMAGE_URL):$(VERSION)
	sudo docker push $(DOCKER_IMAGE_URL):latest
	sudo docker rmi $(DOCKER_IMAGE_URL):latest
	sudo docker rmi $(DOCKER_IMAGE_URL):$(VERSION)
	sudo docker rmi $(MODULENAME):$(VERSION)
build:
	$(GOBUILD) -o bin/$(MODULENAME).exe cmd/$(MODULENAME)/main.go
run:
	$(GOBUILD) -o bin/$(MODULENAME).exe cmd/$(MODULENAME)/main.go
	./bin/$(MODULENAME).exe
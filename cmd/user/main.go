package main

import (
	"fmt"
	"gitee.com/jiransoft/core/log"
	"os"
	"os/signal"
	"syscall"
	"gitee.com/jiransoft/user/internal/user/grpc"
	"gitee.com/jiransoft/user/internal/user/http"
)

func main()  {
	httpSrv := http.New()
	grpc.New()
	// signal
	c := make(chan os.Signal, 1)
	signal.Notify(c, syscall.SIGHUP, syscall.SIGQUIT, syscall.SIGTERM, syscall.SIGINT)
	for {
		s := <-c
		log.GetLoger().Info(fmt.Sprintf("scrm get a signal %s", s.String()))
		switch s {
		case syscall.SIGQUIT, syscall.SIGTERM, syscall.SIGINT:
			httpSrv.Close()
			log.GetLoger().Info("scrm exit")
			return
		case syscall.SIGHUP:
		default:
			return
		}
	}
}

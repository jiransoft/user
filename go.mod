module gitee.com/jiransoft/user

go 1.16

require (
	gitee.com/jiransoft/core v0.0.13
	gitee.com/jiransoft/w2 v0.0.31
	github.com/gin-gonic/gin v1.6.3
	github.com/golang/glog v0.0.0-20210429001901-424d2337a529
	github.com/google/uuid v1.3.0
	github.com/patrickmn/go-cache v2.1.0+incompatible
	google.golang.org/grpc v1.27.0
	google.golang.org/protobuf v1.26.0
)

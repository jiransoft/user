package grpc

import (
	"fmt"
	"gitee.com/jiransoft/core/conf"
	"google.golang.org/grpc"
	"net"
	"gitee.com/jiransoft/user/pkg/proto/user"
)

func New()  {
	addr := conf.GetConf().SysSetting.RpcAddr
	listener, err := net.Listen("tcp", ":"+addr)
	if err != nil{
		panic(err)
	}
	rpcServer := grpc.NewServer()
	user.RegisterUserServer(rpcServer,&User{})
	//reflection.Register(rpcServer)
	fmt.Println("grpc:监听端口："+addr)
	go func() {
		if err = rpcServer.Serve(listener); err != nil {
			panic(err)
		}
	}()
}
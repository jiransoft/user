package grpc

import (
	"context"
	"errors"
	"gitee.com/jiransoft/user/internal/user/logic"
	"gitee.com/jiransoft/user/pkg/proto/user"
)

type User struct {}

func (u *User)GetSessionInfo(ctx context.Context,code *user.SessionReq) (*user.SessionInfo,error) {
	s := logic.GetSession(code.Session)
	if s == nil {
		return nil,errors.New("session已过期")
	}
	//todo 权限/角色
	us := &user.SessionInfo{
		SessionId:s.SessionId,
		CreateTime:s.CreateTime.Unix(),
		UpdateTime:s.UpdateTime.Unix(),
		ExpiredTime:s.ExpiredTime.Unix(),
		UserId:s.UserId,
		UserName:s.UserName,
		Policy:s.Policy,
	}
	return us,nil
}

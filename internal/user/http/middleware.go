package http

import (
	"fmt"
	"gitee.com/jiransoft/core/conf"
	"gitee.com/jiransoft/core/httpx"
	"gitee.com/jiransoft/core/utils"
	"gitee.com/jiransoft/w2"
	"net/http"
	"net/http/httputil"
	"runtime"
	"time"
	"gitee.com/jiransoft/user/internal/user/logic"

	"github.com/gin-gonic/gin"
	log "github.com/golang/glog"
)

// 处理跨域请求,支持options访问
func Cors() gin.HandlerFunc {
	return func(c *gin.Context) {
		method := c.Request.Method
		origin := c.GetHeader("Origin")
		c.Header("Access-Control-Allow-Origin", utils.IF(origin == "", "*", origin).(string))
		c.Header("Access-Control-Allow-Headers", "Content-Type,AccessToken,X-CSRF-Token, Authorization, Token, showLoading, loadingTarget, showError, returnData")
		c.Header("Access-Control-Allow-Methods", "POST, GET, OPTIONS")
		c.Header("Access-Control-Expose-Headers", "Content-Length, Access-Control-Allow-Origin, Access-Control-Allow-Headers, Content-Type")
		c.Header("Access-Control-Allow-Credentials", "true")

		// 放行所有OPTIONS方法
		if method == "OPTIONS" {
			c.AbortWithStatus(http.StatusNoContent)
		}
		// 处理请求
		c.Next()
	}
}

func recoverHandler(c *gin.Context) {
	defer func() {
		if err := recover(); err != nil {
			const size = 64 << 10
			buf := make([]byte, size)
			buf = buf[:runtime.Stack(buf, false)]
			httprequest, _ := httputil.DumpRequest(c.Request, false)
			pnc := fmt.Sprintf("[Recovery] %s panic recovered:\n%s\n%s\n%s", time.Now().Format("2006-01-02 15:04:05"), string(httprequest), err, buf)
			fmt.Print(pnc)
			log.Error(pnc)
			c.AbortWithStatus(500)
		}
	}()
	c.Next()
}

func GetSessionName() string {
	return conf.GetConf().Setting.GetStringd("session_name","geeran")
}
func generate_session_w2(u *w2.W2LoginUser, icontinue string, c *gin.Context) string {
	session := logic.GenerateSessionW2(u)
	c.SetCookie(GetSessionName(), session, 12*60*60, "/", "", false, true)
	return session
}
func w2_logout(c *gin.Context)  {
	sessionHandler(c)
	s := getContextSession(c)
	logic.RemoveSession(s.SessionId)
	c.SetCookie(GetSessionName(), "", 12*60*60, "/", "", false, true)
	httpx.Ok(c)
}

func sessionHandler(c *gin.Context) {
	session_id, _ := c.Cookie(GetSessionName())
	if session_id == "" {
		session_id = c.Request.Header.Get("Authorization")
	}
	if session_id == "" {
		httpx.FailWithDetailed(c, httpx.LoginFailure, "请登陆", nil)
		return
	}
	s := logic.GetSession(session_id)
	if s == nil {
		httpx.FailWithDetailed(c, httpx.LoginFailure, "请重新登陆", nil)
		return
	}
	setContextSession(c, s)
	c.Next()
}

func setContextSession(c *gin.Context, s *logic.Session) {
	c.Set("session", s)
}
func getContextSession(c *gin.Context) *logic.Session {
	return c.MustGet("session").(*logic.Session)
}

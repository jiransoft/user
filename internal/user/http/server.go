package http

import (
	"context"
	"fmt"
	"gitee.com/jiransoft/core/conf"
	"gitee.com/jiransoft/core/middleware"
	"gitee.com/jiransoft/w2"
	"github.com/gin-gonic/gin"
	"net/http"
	"time"
)

// Server is http server.
type Server struct {
	router *gin.Engine
	server *http.Server
}

// New new a http server.
func New() *Server {
	s := &Server{}

	s.router = gin.New()
	s.router.Use(middleware.EnableGinLog(), recoverHandler, Cors())
	s.initRouter()

	s.server = &http.Server{
		Addr:    ":" + conf.GetConf().SysSetting.HttpAddr,
		Handler: s.router,
	}

	go func() {
		if err := s.server.ListenAndServe(); err != nil {
			panic(err)
		}
	}()

	return s
}

func (s *Server) initRouter() {
	InitStaffRouter(s.router)
	w2.InitW2LoginRouter(s.router.Group(""), generate_session_w2,w2_logout)
}

// Close close the server.
// todo 待测试服务优雅退出
func (s *Server) Close() {
	timeoutCtx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := s.server.Shutdown(timeoutCtx); err != nil {
		fmt.Println(err)
	}
}

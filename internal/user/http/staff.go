package http

import (
	"gitee.com/jiransoft/core/conf"
	"gitee.com/jiransoft/core/httpx"
	"github.com/gin-gonic/gin"
	"gitee.com/jiransoft/user/internal/user/logic"
)

func InitStaffRouter(e *gin.Engine) {
	r := e.Group("staff")
	{
		r.GET("select", GetStaffSelect)
		r.GET("org/select", GetOrgSelect)
		r.Use(sessionHandler)
		r.GET("session_info", GetStaffSessionInfo)
		r.POST("session_info", GetStaffSessionInfo)
		r.POST("logout", StaffLogout)
	}
}

func GetStaffSelect(c *gin.Context) {
	name := c.DefaultQuery("name", "")
	data := logic.GetStaffSelect(name)
	httpx.OkWithData(c, data)
}
func GetOrgSelect(c *gin.Context) {
	name := c.DefaultQuery("name", "")
	data := logic.GetOrgSelect(name)
	httpx.OkWithData(c, data)
}
func GetStaffSessionInfo(c *gin.Context) {
	s := getContextSession(c)
	httpx.OkWithData(c, s)
}
func StaffLogout(c *gin.Context) {
	s := getContextSession(c)
	logic.RemoveSession(s.SessionId)
	c.SetCookie("geeran-scrm", "", 12*60*60, "/", "", false, true)
	//退出w2的登录,正确则返回重定向的地址，错误返回错误信息
	if mes, isOk := logoutWithW2(c); isOk {
		httpx.OkWithData(c, mes)
	} else {
		httpx.FailWithMessage(c, mes)
	}
}

//退出w2系统登录
func logoutWithW2(c *gin.Context) (message string, isOk bool) {
	sso_url := conf.GetConf().Setting.GetChildd("w2").GetStringd("sso_url", "")
	if sso_url == "" {
		return "sso地址未配置，请联系系统管理员", false
	}
	sso_url = sso_url + "?act=logout"
	return sso_url, true
}

package logic

import (
	"gitee.com/jiransoft/w2"
	"github.com/google/uuid"
	"github.com/patrickmn/go-cache"
	"time"
)

type Session struct {
	SessionId			string
	CreateTime			time.Time
	UpdateTime			time.Time
	ExpiredTime			time.Time
	//*w2.W2LoginUser		//todo 移除
	UserId				string
	UserName			string
	Policy				string
}

var session_cache = cache.New(12*time.Hour,13*time.Hour)

//生成session
func GenerateSessionW2(u *w2.W2LoginUser) string {
	if s,ok := session_cache.Get("uid:"+u.Id);ok{
		return s.(*Session).SessionId
	}
	s := &Session{
		SessionId:uuid.New().String(),
		CreateTime:time.Now(),
		UpdateTime:time.Now(),
		ExpiredTime:time.Now().Add(12*time.Hour),
		UserId:u.Id,
		UserName:u.Name,
		Policy:u.Policy,
	}
	session_cache.Set("uid:"+u.Id,s,cache.DefaultExpiration)
	session_cache.Set("sid:"+s.SessionId,s,cache.DefaultExpiration)
	return s.SessionId
}

func GetSession(sid string) *Session {
	s,ok := session_cache.Get("sid:"+sid)
	if ok {
		return s.(*Session)
	}
	return nil
}
func RemoveSession(sid string)  {
	s := GetSession(sid)
	if s != nil {
		session_cache.Delete("sid:"+s.SessionId)
		session_cache.Delete("uid:"+s.UserId)
	}
}
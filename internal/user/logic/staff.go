package logic

import (
	"gitee.com/jiransoft/w2"
	"strings"
	"gitee.com/jiransoft/user/internal/user/model/response"
)


//员工选择器
func GetStaffSelect(name string) (*[]response.StaffSelect) {
	resp := []response.StaffSelect{}
	w := w2.New()
	ps := w.GetAllPeople()
	for _,p := range *ps {
		if strings.Contains(p.Name,name) {
			resp = append(resp, response.StaffSelect{
				Id:p.ID,
				Name:p.Name,
				FullDeptName:p.GetFullDeptName(),
			})
			if len(resp) > 50 {
				break
			}
		}
	}
	return &resp
}
func GetOrgSelect(name string) *[]response.SelectData {
	resp := &[]response.SelectData{}
	w := w2.New()
	select_staff_name(w,name,resp)
	select_dept_name(w,name,resp)
	return resp
}
func select_staff_name(w *w2.W2OC,name string,resp *[]response.SelectData) {
	if len(*resp) > 50 {
		return
	}
	ps := w.GetAllPeople()
	for _,p := range *ps {
		if strings.Contains(p.Name,name) {
			*resp = append(*resp, response.SelectData{
				Key:p.ID,
				Value:p.Name,
				Other1:p.GetFullDeptName(),
				Other2:"user",
			})
			if len(*resp) > 50 {
				break
			}
		}
	}
	return
}
func select_dept_name(w *w2.W2OC,name string,resp *[]response.SelectData) {
	if len(*resp) > 50 {
		return
	}
	ds := w.GetAllDept()
	for _,d := range ds {
		if strings.Contains(d.Name,name) {
			*resp = append(*resp, response.SelectData{
				Key:d.ID,
				Value:d.Name,
				Other1:d.GetFullDeptName(),
				Other2:"dept",
			})
			if len(*resp) > 50 {
				break
			}
		}
	}
	return
}
package response

type SelectData struct {
	Key			string			`json:"key"`
	Value		string			`json:"value"`
	Other1		string			`json:"other1"`
	Other2		string			`json:"other2"`
	Other3		string			`json:"other3"`
	Other4		string			`json:"other4"`
}

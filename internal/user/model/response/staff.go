package response

type StaffSelect struct {
	Id 			string			`json:"id"`
	Name 		string			`json:"name"`
	FullDeptName 	string		`json:"full_dept_name"`
}

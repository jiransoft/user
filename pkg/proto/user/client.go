package user

import (
	"context"
	grpc2 "gitee.com/jiransoft/user/pkg/grpc"
	"google.golang.org/grpc"
)

const GrpcName  = "user"

func GetSessionInfo(ctx context.Context,session_id string) (*SessionInfo,error) {
	conn, err := grpc.Dial(grpc2.GetTraefikTarget(GrpcName), grpc.WithInsecure(),grpc.WithResolvers(&grpc2.TraefikBuilder{}))
	if err != nil {
		return nil,err
	}
	defer conn.Close()
	c := NewUserClient(conn)
	r,err := c.GetSessionInfo(ctx,&SessionReq{Session:session_id})
	if err != nil {
		return nil,err
	}
	return r,nil
}
func (s *SessionInfo)GetRole(module_name string) *Role {
	return GetRole(s,module_name)
}
package user

import (
	"gitee.com/jiransoft/w2"
	"github.com/patrickmn/go-cache"
	"time"
)

//------------------------------w2权限----------------------------------------
var role_cache = cache.New(1*time.Hour,1*time.Hour)

type Role map[string]string
func (r *Role)Get(k string) string {
	if r == nil {
		return ""
	}
	if t,ok := (*r)[k];ok {
		return t
	}
	return ""
}
func (r *Role)Check(role string) bool {
	result := r.Get(role)
	if result == "1" {
		return true
	}
	return false
}

func GetRole(info *SessionInfo,module_name string) *Role {
	if info == nil {
		return nil
	}
	v,ok := role_cache.Get(info.SessionId+module_name)
	if ok {
		return v.(*Role)
	}
	return ParseW2RoleStr(info.Policy,module_name)
}
func ParseW2Role(p *w2.Policy,module_name string) *Role {
	r := Role{}
	for _,t := range p.Items {
		if t.Id == module_name {
			for _,b := range t.Bool {
				r[b.Id] = b.Value
			}
		}
	}
	return &r
}
func ParseW2RoleStr(pstr string,module_name string) *Role {
	p,_ := w2.ParsePolicy(pstr)
	return ParseW2Role(p,module_name)
}
func CheckRole(r *Role,role string) bool {
	result := r.Get(role)
	if result == "1" {
		return true
	}
	return false
}